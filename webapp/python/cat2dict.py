# -*- coding: utf-8 -*-
import csv


def get_parent(cat_id):
    category = categories[cat_id].copy()
    if category['parent_id'] != 0:
        p = get_parent(category['parent_id'])
        category['parent_category_name'] = categories[p['id']]['category_name']
    return category


categories = {}
print('CATEGORIES = [')
with open('categories.tsv') as f:
    f = csv.DictReader(f, delimiter='\t')
    for row in f:
        row['id'] = int(row['id'])
        row['parent_id'] = int(row['parent_id'])
        print('    %s,' % dict(row))
        categories[row['id']] = dict(row)
print(']')

print('PARENTS = {')
for i in sorted(categories):
    print('    %i: %s,' % (i, get_parent(i)))
print('}')
